package com.example.nativenetworktools.home

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableOnSubscribe
import io.reactivex.rxjava3.schedulers.Schedulers

class HomeViewmodel : ViewModel() {

    //setter getter untuk meyimpan data yang sudah diperoleh
    private val _networkResults = MutableLiveData<Pair<String, Int>>()
    val networkResults: LiveData<Pair<String, Int>>
        get() = _networkResults


    //fungsi observable untuk mengakses fungsi utama dikelas homefunction
    fun network(context: Context) {
        Observable.create(ObservableOnSubscribe<Pair<String, Int>> { emitter ->
            //memanggil fungsi
            val resultPair = HomeFunction().networkType(context)
            //mengirimkan return value
            emitter.onNext(resultPair)
            //memberitau jika proses sudah selesai
            emitter.onComplete()
        })
            //memastikan fungsi berjalan pada thread terpisah dari thread utama
            .subscribeOn(Schedulers.io())
            //ketika proses sudah selesai maka proses akan kembali menjadi dengan thread utama
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { resultPair ->
                _networkResults.postValue(resultPair)
                val (str, num) = resultPair
            }
    }

    private val _ipaddressResults = MutableLiveData<String>()
    val ipaddressResults: LiveData<String>
        get() = _ipaddressResults

    fun ipAddress() {
        Observable.create(ObservableOnSubscribe<String> { emitter ->
            val signalResult = HomeFunction().getIpAddress()
            emitter.onNext(signalResult)
            emitter.onComplete()
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                _ipaddressResults.postValue(it)
            }
    }

}