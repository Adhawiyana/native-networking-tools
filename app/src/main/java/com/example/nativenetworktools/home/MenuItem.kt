package com.example.nativenetworktools.home

import android.view.View
import com.example.nativenetworktools.R
import com.example.nativenetworktools.databinding.ItemButtonBinding
import com.example.nativenetworktools.dummy.dummy
import com.xwray.groupie.viewbinding.BindableItem

class MenuItem (val actions: dummy, private val listener : onMove ) : BindableItem<ItemButtonBinding>() {
    override fun bind(viewBinding: ItemButtonBinding, position: Int) {
        viewBinding.txtTextName.text = actions.actionName
        viewBinding.imgMenu.setImageResource(actions.images)
        viewBinding.txtDesc.text = actions.description
        viewBinding.btnIcons.setOnClickListener {
            listener.moveItem(position)
        }
    }

    override fun getLayout(): Int {
        return R.layout.item_button
    }

    override fun initializeViewBinding(view: View): ItemButtonBinding {
        return ItemButtonBinding.bind(view);
    }

    interface onMove{
        fun moveItem (position: Int)
    }
}