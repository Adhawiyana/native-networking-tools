package com.example.nativenetworktools.home

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.telephony.TelephonyManager
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import java.net.Inet4Address
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.Collections

class HomeFunction() {

    //fungsi untuk mendapatkan jenis jaringan dan mmengukur jeringan dalam satuan dBm.
    fun networkType(context: Context) : Pair<String, Int>{
        //variabel utama untuk meyimpan dua nilai dalam bentuk string dan int
        var signal = 0
        var networkType = "Unknown"
        //mendapatkan ConnectivityManager service untuk setting jaringan
        val connection = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val network = connection.activeNetwork
        val caps = connection.getNetworkCapabilities(network)

        //cara menentukan jenis jaringan
        if(caps != null){
            when{
                //jenis jaringan bertipe wifi
                caps.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    // jenis jaringan untuk tipe wifi
                    val wifiSignal = caps.signalStrength
                    signal = wifiSignal
                    networkType = "Wi-Fi Connected"
                }
                caps.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    if(ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(context as Activity, arrayOf(Manifest.permission.ACCESS_NETWORK_STATE), 1)
                    }else{
                        //jenis jaringan bertipe 2G
                        val phone = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                        when(phone.dataNetworkType){
                            TelephonyManager.NETWORK_TYPE_GPRS,
                            TelephonyManager.NETWORK_TYPE_EDGE,
                            TelephonyManager.NETWORK_TYPE_CDMA,
                            TelephonyManager.NETWORK_TYPE_1xRTT,
                            TelephonyManager.NETWORK_TYPE_IDEN -> {
                                //kekuatan jaringan bertipe 2G
                                networkType = "2G Connected"
                                val info = phone.signalStrength?.cellSignalStrengths?.get(0)
                                info.let {
                                    if (it != null){
                                        signal = it.dbm
                                    }
                                }
                            }

                            //jenis jaringan bertipe 3G
                            TelephonyManager.NETWORK_TYPE_UMTS,
                            TelephonyManager.NETWORK_TYPE_EVDO_0,
                            TelephonyManager.NETWORK_TYPE_EVDO_A,
                            TelephonyManager.NETWORK_TYPE_HSDPA,
                            TelephonyManager.NETWORK_TYPE_HSUPA,
                            TelephonyManager.NETWORK_TYPE_HSPA,
                            TelephonyManager.NETWORK_TYPE_EVDO_B,
                            TelephonyManager.NETWORK_TYPE_EHRPD,
                            TelephonyManager.NETWORK_TYPE_HSPAP -> {
                                //kekuatan jaringan bertipe 3G
                                networkType = "3G Connected"
                                val info = phone.signalStrength?.cellSignalStrengths?.get(0)
                                info.let {
                                    if (it != null){
                                        signal = it.dbm
                                    }
                                }
                            }

                            //jenis jaringan bertipe 4G
                            TelephonyManager.NETWORK_TYPE_LTE -> {
                                //kekuatan jaringan bertipe 4G
                                networkType = "4G Connected"
                                val info = phone.signalStrength?.cellSignalStrengths?.get(0)
                                info.let {
                                    if (it != null){
                                        signal = it.dbm
                                    }
                                }
                            }

                            //jenis jaringan bertipe 5G
                            TelephonyManager.NETWORK_TYPE_NR -> {
                                //kekuatan jaringan bertipe 5G
                                networkType = "5G Connected"
                                val info = phone.signalStrength?.cellSignalStrengths?.get(0)
                                info.let {
                                    if (it != null){
                                        signal = it.dbm
                                    }
                                }
                            }

                            else -> {
                                Log.d("Connection => ", "known")
                            }
                        }
                    }
                }
                else -> {
                    Log.d("Connection => ", "Unknown")
                }
            }
        }else{
            Log.d("Connection => ", "Something wrong")
        }
        //return value untuk jenis jaringan dan signal strength
        return Pair(networkType, signal)
    }

    fun getIpAddress() : String {
        var ipAddressResult = "**.**.**.**"
        try {
            val interfaces: List<NetworkInterface> = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (intf in interfaces) {
                val addrs: List<InetAddress> = Collections.list(intf.inetAddresses)
                for (addr in addrs) {
                    if (!addr.isLinkLocalAddress && !addr.isLoopbackAddress && addr is Inet4Address) {
                        ipAddressResult = addr.hostAddress!!
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ipAddressResult
    }
}