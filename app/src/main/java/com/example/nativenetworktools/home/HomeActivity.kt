package com.example.nativenetworktools.home

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.TelephonyManager
import android.util.Log
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nativenetworktools.databinding.ActivityHomeBinding
import com.example.nativenetworktools.dummy.dummy
import com.example.nativenetworktools.network.NetworkActivity
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import io.reactivex.rxjava3.disposables.CompositeDisposable

class HomeActivity : AppCompatActivity() {

    //varibel untuk kode akses izin fitur
    private val REQUEST_CODE = 1
    private lateinit var phone: TelephonyManager
    private lateinit var pages : dummy.actions
    private var groupadapter : GroupAdapter<GroupieViewHolder> = GroupAdapter()

    private lateinit var binding : ActivityHomeBinding
    //akses viewmodel untuk fingsi ke kelas viewmodel
    private val viewModel: HomeViewmodel by viewModels()
    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //seleksi kondisi untuk memeriksa semua izin fitur sudah dipatkan atau belum
        if (ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED
            || ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            || ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
            || ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_WIFI_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_WIFI_STATE), REQUEST_CODE)
        } else {
            Log.d("Permission => ", "Allowed")
        }
        //variabel dengan akses layanan ke telephony manager
        phone = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

        pages = dummy.actions
        pages.listaction.forEach {
            groupadapter.add(MenuItem(it, object : MenuItem.onMove{
                override fun moveItem(position: Int) {
                    val intent = Intent(this@HomeActivity, it.actionPage)
                    startActivity(intent)
                }
            }))
        }

        //akses viewmodel untuk menjalankan fungsi network type dan signal strength
        viewModel.network(applicationContext)
        viewModel.ipAddress()
        //akses viewmodel untuk observe value yang sudah didapat fungsi utama
        viewModel.networkResults.observe(this) {result ->
            if(result != null){
                result.let { network ->
                    binding.txtConnection.text = "${network.first}"
                    viewModel.ipaddressResults.observe(this) {ipresults ->
                        if(ipresults != null){
                            ipresults.let {ip ->
                                binding.details.text = "$ip | ${network.second} dBm"
                            }
                        }
                    }
                }
            }
        }

        initrv()

        binding.textView.setOnClickListener {
            val testing = Intent(applicationContext, NetworkActivity::class.java)
            startActivity(testing)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    private fun initrv(){
        binding.recycleview.apply {
            layoutManager = LinearLayoutManager(this@HomeActivity, LinearLayoutManager.HORIZONTAL, false)
            adapter = groupadapter
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if(requestCode == REQUEST_CODE){
            if(grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Log.d("Permission", "Granted")
            }else{
                Log.d("Permission", "Denied")
            }
        }
    }
}