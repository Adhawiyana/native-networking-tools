package com.example.nativenetworktools.ping

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.ObservableOnSubscribe
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers

class PingViewmodel: ViewModel() {
    //variable dasar untuk pembatalan proses
    private var disposable: Disposable? = null

    //setter getter untuk meyimpan data yang sudah diperoleh
    private val _pingResults = MutableLiveData<List<String>>()
    val pingResults: LiveData<List<String>>
        get() = _pingResults

    //fungsi observable untuk mengakses fungsi utama dikelas pingfunction
    fun ping(site: String) {
        Observable.create(ObservableOnSubscribe<List<String>> { emitter ->
            val results = PingFunction().pingTesting(site)
            emitter.onNext(results)
            emitter.onComplete()
        })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { results ->
                _pingResults.postValue(results)
            }
    }

    //fungsi untuk pembatalan proses
    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
    }
}