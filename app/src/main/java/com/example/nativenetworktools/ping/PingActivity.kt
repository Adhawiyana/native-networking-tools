package com.example.nativenetworktools.ping

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nativenetworktools.databinding.ActivityPingBinding
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder

class PingActivity : AppCompatActivity() {

    //metode viewbinding untuk akses ui pada file xml
    private lateinit var binding : ActivityPingBinding
    //akses viewmodel untuk fingsi ke kelas viewmodel
    private val viewModel: PingViewmodel by viewModels()
    //variable untuk adapter recyclerview
    private var groupadapter : GroupAdapter<GroupieViewHolder> = GroupAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPingBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        //button function untuk menjalankan fungsi ping test pada viewmodel
        binding.btnPing.setOnClickListener {
            viewModel.ping(binding.edtInputPing.text.toString())
        }

        //akses viewmodel untuk melakukan obverservasi pada fungsi ping test
        viewModel.pingResults.observe(this) { result ->
            if (result.isNotEmpty()) {
                result.forEach {
                    groupadapter.add(PingItem(it))
                }
            }
        }

        initrv()
    }

    //fungsi untuk memanggil recyvlerview adapter yang sudah dibuat
    fun initrv(){
        binding.pingresult.apply {
            layoutManager = LinearLayoutManager(this@PingActivity)
            adapter = groupadapter
        }
    }
}