package com.example.nativenetworktools.ping

import android.view.View
import com.example.nativenetworktools.R
import com.example.nativenetworktools.databinding.PingitemBinding
import com.xwray.groupie.viewbinding.BindableItem

//adapter recyclerview untuk hasil dari ping test
class PingItem(val result : String) : BindableItem<PingitemBinding>() {
    //binding view untuk menampilkan data
    override fun bind(viewBinding: PingitemBinding, position: Int) {
        viewBinding.pingResult.text = result
    }

    //get layout dari xml file
    override fun getLayout(): Int {
        return R.layout.pingitem
    }

    //inisial layout dan view untuk recyclerview
    override fun initializeViewBinding(view: View): PingitemBinding {
        return PingitemBinding.bind(view)
    }
}