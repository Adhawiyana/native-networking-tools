package com.example.nativenetworktools.ping

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

class PingFunction {

    //fungsi utama ping test
    fun pingTesting(site: String): List<String> {
        //ping test command
        val command = "/system/bin/ping -c 5 $site"
        //variabel untuk return data dalam bentuk list
        val results = mutableListOf<String>()

        try {
            //metode untuk running command
            val process = Runtime.getRuntime().exec(command)
            //untuk mendapatkan hasil atau value dari ping test
            val reader = BufferedReader(InputStreamReader(process.inputStream))

            var line: String?
            //untuk membaca hasil dari return value ping test
            while (reader.readLine().also { line = it } != null) {
                val packageNum = line?.substringAfter("icmp_seq=")?.substringBefore(" ")
                val time = line?.substringAfter("time=")?.substringBefore(" ms")
                val pingResult = "Package $packageNum received in $time ms\n"
                if (pingResult.isNotEmpty()){
                    results.add(pingResult)
                }
            }

            val exitCode = process.waitFor()
            println("Ping test exited with code: $exitCode")
        //error handling pada fungsi ping jika ada
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        //return value pada fungsi ping test
        return results
    }

}