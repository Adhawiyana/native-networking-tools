package com.example.nativenetworktools

import android.app.Application
import android.util.Log
import com.example.nativenetworktools.modules.UtilsModules
import com.example.nativenetworktools.modules.ViewmodelModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidContext(this@App)
            modules(UtilsModules, ViewmodelModules)
        }

        if(BuildConfig.DEBUG) Log.d("Debug => ", "something is here")
    }
}