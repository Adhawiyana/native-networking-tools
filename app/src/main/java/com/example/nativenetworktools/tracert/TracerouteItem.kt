package com.example.nativenetworktools.tracert

import android.view.View
import com.example.nativenetworktools.R
import com.example.nativenetworktools.databinding.PingitemBinding
import com.xwray.groupie.viewbinding.BindableItem

class TracerouteItem(val textresult : String):BindableItem<PingitemBinding>() {
    override fun bind(viewBinding: PingitemBinding, position: Int) {
        viewBinding.pingResult.text = textresult
    }

    override fun getLayout(): Int {
        return R.layout.pingitem
    }

    override fun initializeViewBinding(view: View): PingitemBinding {
        return PingitemBinding.bind(view)
    }
}