package com.example.nativenetworktools.tracert

import android.content.Context
import android.util.Log
import tej.androidnetworktools.lib.Route
import tej.androidnetworktools.lib.scanner.OnTracerouteListener
import tej.androidnetworktools.lib.scanner.Traceroute

class TracerouteFunction {

    fun traceroute(site: String, context: Context): List<String> {
        Traceroute.init(context)
        val results = mutableListOf<String>()
        Traceroute.start(site, object : OnTracerouteListener {
            override fun onRouteAdd(route: Route) {
                Log.d("onRouteAdd", "Ip Address -> ${route.ipAddress}, Raw Address -> ${route.rawAddress}")
            }

            override fun onComplete(routes: List<Route>) {
                for (data in routes){
                    val tracertresult = "Ip Address ${data.ipAddress} Raw Ip ${data.rawAddress} \n"
                    results.add(tracertresult)
                }
            }

            override fun onFailed() {
                Log.d("onFailed", "traceroute failed")
            }
        })

        return results
    }
}