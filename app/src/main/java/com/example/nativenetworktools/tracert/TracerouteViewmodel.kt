package com.example.nativenetworktools.tracert

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import tej.androidnetworktools.lib.Route
import tej.androidnetworktools.lib.scanner.OnTracerouteListener
import tej.androidnetworktools.lib.scanner.Traceroute

class TracerouteViewmodel : ViewModel() {

    private val _tracerrouteresults = MutableLiveData<List<String>>()
    val tracerrouteresults: LiveData<List<String>>
        get() = _tracerrouteresults

    fun traceroute(site: String, context: Context){
        Traceroute.init(context)
        val results = mutableListOf<String>()
        Traceroute.start(site, object : OnTracerouteListener {
            override fun onRouteAdd(route: Route) {
                Log.d("onRouteAdd", "Ip Address -> ${route.ipAddress}, Raw Address -> ${route.rawAddress}")
            }

            override fun onComplete(routes: List<Route>) {
                for (data in routes){
                    val tracertresult = "Ip Address ${data.ipAddress} Raw Ip ${data.rawAddress} \n"
                    results.add(tracertresult)
                }
                _tracerrouteresults.postValue(results)
            }

            override fun onFailed() {
                Log.d("onFailed", "traceroute failed")
            }
        })
    }

    override fun onCleared() {
        super.onCleared()
    }
}