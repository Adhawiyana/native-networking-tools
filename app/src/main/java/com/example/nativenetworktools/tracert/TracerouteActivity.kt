package com.example.nativenetworktools.tracert

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nativenetworktools.databinding.ActivityTracerouteBinding
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder


class TracerouteActivity : AppCompatActivity() {
    private lateinit var binding : ActivityTracerouteBinding
    private val viewModel: TracerouteViewmodel by viewModels()
    private var groupadapter : GroupAdapter<GroupieViewHolder> = GroupAdapter()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTracerouteBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        binding.btnTracert.setOnClickListener {
            viewModel.traceroute(binding.edtInputTracert.text.toString(), applicationContext)
        }

        viewModel.tracerrouteresults.observe(this){result ->
            if(result.isNotEmpty()){
                groupadapter.clear()
                result.forEach {
                    groupadapter.add(TracerouteItem(it))
                }
            }
        }

        initrv()
    }

    fun initrv(){
        binding.traceresult.apply {
            layoutManager = LinearLayoutManager(this@TracerouteActivity)
            adapter = groupadapter
        }
    }
}