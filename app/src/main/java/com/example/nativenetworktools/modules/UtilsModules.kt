package com.example.nativenetworktools.modules

import com.example.nativenetworktools.home.HomeFunction
import com.example.nativenetworktools.ping.PingFunction
import com.example.nativenetworktools.tracert.TracerouteFunction
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import org.koin.dsl.module

val UtilsModules = module {
    factory { GroupAdapter<GroupieViewHolder>() }
    single { PingFunction() }
    single { TracerouteFunction() }
    single { HomeFunction() }
}