package com.example.nativenetworktools.modules

import com.example.nativenetworktools.home.HomeViewmodel
import com.example.nativenetworktools.ping.PingViewmodel
import com.example.nativenetworktools.tracert.TracerouteViewmodel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val ViewmodelModules = module {
    viewModel { PingViewmodel() }
    viewModel { TracerouteViewmodel() }
    viewModel { HomeViewmodel() }
}