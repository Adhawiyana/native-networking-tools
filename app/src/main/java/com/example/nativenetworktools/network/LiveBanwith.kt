package com.example.nativenetworktools.network

import android.net.TrafficStats
import android.os.Handler
import android.os.Looper
import java.util.Timer
import java.util.TimerTask

class LiveBanwith(private val interval: Long = 1000, private val listener: BandwidthListener) {
    private var handler: Handler? = null
    private val timer = Timer()
    private var lastTimeStamp = System.currentTimeMillis()
    private var lastRxBytes: Long = 0
    private var lastTxBytes: Long = 0

    interface BandwidthListener {
        fun onBandwidthUpdate(download: Long, upload: Long)
    }

    fun start() {
        handler = Handler(Looper.getMainLooper())
        timer.schedule(object : TimerTask() {
            override fun run() {
                handler?.post {
                    val now = System.currentTimeMillis()
                    val timeDelta = now - lastTimeStamp
                    if (timeDelta > 0) {
                        val rxBytes = TrafficStats.getTotalRxBytes() - TrafficStats.getMobileRxBytes()
                        val txBytes = TrafficStats.getTotalTxBytes() - TrafficStats.getMobileTxBytes()
                        val rxSpeed = (rxBytes - lastRxBytes) / timeDelta * 1000
                        val txSpeed = (txBytes - lastTxBytes) / timeDelta * 1000
                        listener.onBandwidthUpdate(rxSpeed, txSpeed)
                        lastRxBytes = rxBytes
                        lastTxBytes = txBytes
                        lastTimeStamp = now
                    }
                }
            }
        }, interval, interval)
    }

    fun stop() {
        timer.cancel()
        handler?.removeCallbacksAndMessages(null)
    }
}