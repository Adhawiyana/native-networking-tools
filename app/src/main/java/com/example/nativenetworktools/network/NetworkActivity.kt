package com.example.nativenetworktools.network

import android.os.Bundle
import android.telephony.*
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.nativenetworktools.databinding.ActivityNetworkBinding


class NetworkActivity : AppCompatActivity() {


    private lateinit var binding : ActivityNetworkBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNetworkBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val bandwidthMeter = LiveBanwith(1000, object : LiveBanwith.BandwidthListener {
            override fun onBandwidthUpdate(download: Long, upload: Long) {
                val downloadMbps = download.toDouble() / 125000.0
                val uploadMbps = upload.toDouble() / 125000.0
                Log.d("BandwidthMeter", "Download: $downloadMbps Mbps, Upload: $uploadMbps Mbps")
            }
        })

        binding.btnNetwork.setOnClickListener {
            bandwidthMeter.start()
        }
    }

}