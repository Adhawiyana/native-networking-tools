package com.example.nativenetworktools.dummy

import com.example.nativenetworktools.R
import com.example.nativenetworktools.bandwith.BandwithActivity
import com.example.nativenetworktools.ping.PingActivity
import com.example.nativenetworktools.tracert.TracerouteActivity

data class dummy(
    val actionName : String,
    val actionPage : Class<*>,
    val images : Int,
    val description : String,
    ) {

    object actions{
        val listaction = listOf<dummy>(
            dummy("Ping Test", PingActivity::class.java, R.drawable.item1, "Tes sederhana dengan masukan data berupa nama situs atau IP Address untuk melihat kondisi host di internet"),
            dummy("Traceroute", TracerouteActivity::class.java, R.drawable.item2, "Tes sederhana dengan melihat jumlah router yang dilewati package untuk sampai pada situs tujuan"),
            dummy("Network & Signal", BandwithActivity::class.java, R.drawable.item3, "Mengukur bandwith internet dengan melihat hasil dari tes download dan upload")
        )
    }
}