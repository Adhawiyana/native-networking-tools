package com.example.nativenetworktools.bandwith

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.nativenetworktools.databinding.ActivityBandwithBinding

class BandwithActivity : AppCompatActivity() {

    private lateinit var binding: ActivityBandwithBinding
    private lateinit var downloadTest : DownloadTest

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBandwithBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var progressBarStatus = 0
        var dummy = 0

        downloadTest = DownloadTest()
        binding.btnTest.setOnClickListener {
            Thread(Runnable {
                while (progressBarStatus < 100){
                    try{
                        dummy = dummy + 10
                        Thread.sleep(1000)
                    }catch (e: InterruptedException){
                        e.printStackTrace()
                    }
                    progressBarStatus = dummy
                    binding.downloadProgress.progress = progressBarStatus
                }
            }).start()

//            CoroutineScope(Dispatchers.Main).launch {
//                downloadTest.performUploadTest()
//            }
        }
    }
}