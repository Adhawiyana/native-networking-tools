package com.example.nativenetworktools.bandwith

import android.content.Context
import android.util.Log
import fr.bmartel.speedtest.SpeedTestReport
import fr.bmartel.speedtest.SpeedTestSocket
import fr.bmartel.speedtest.inter.ISpeedTestListener
import fr.bmartel.speedtest.model.SpeedTestError
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withContext
import java.io.*
import java.math.BigDecimal
import java.math.RoundingMode
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.text.DecimalFormat

class DownloadTest {

    suspend fun download(): Double = withContext(Dispatchers.IO) {
        var startTime: Long
        var endTime: Long
        var downloadTime: Long
        var downloadSpeedKbps: Double
        var downloadSpeedMbps: Double
        try {
            val connection = URL("http://speedtest.tele2.net/1MB.zip").openConnection() as HttpURLConnection
            connection.connect()
            startTime = System.currentTimeMillis()
            connection.inputStream.use { input ->
                ByteArray(4096).apply {
                    while (input.read(this) != -1);
                }
            }
            endTime = System.currentTimeMillis()
            downloadTime = endTime - startTime
            downloadSpeedKbps = (1.0 / downloadTime) * 8 * 1024
            downloadSpeedMbps = downloadSpeedKbps/1000
        } catch (e: IOException) {
            throw IOException("Unable to connect to download server. Error message: ${e.message}")
        }
        return@withContext downloadSpeedMbps
    }

    suspend fun upload(): Double = withContext(Dispatchers.IO) {
        val fileSize = 1024 * 1024 // ukuran file dalam byte
        val url = URL("http://speedtest.tele2.net/upload.php")
        val conn = url.openConnection() as HttpURLConnection
        conn.doOutput = true
        conn.requestMethod = "POST"
        conn.setRequestProperty("Connection", "Keep-Alive")
        conn.setRequestProperty("Cache-Control", "no-cache")
        conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=*****")
        val outputStream: OutputStream = conn.outputStream
        val startTime = System.currentTimeMillis()
        for (i in 1..fileSize) {
            outputStream.write(0)
        }
        outputStream.flush()
        outputStream.close()
        val endTime = System.currentTimeMillis()
        val duration = (endTime - startTime) / 1000.0 // durasi dalam detik
        val uploadSpeed = (fileSize / duration) / (1024 * 1024) // kecepatan dalam Mbps
        return@withContext uploadSpeed
    }

    suspend fun performDownloadTest(): Double = withContext(Dispatchers.IO) {
        val speedTestSocket = SpeedTestSocket()

        val downloadSpeed = suspendCancellableCoroutine<Double> { continuation ->
            speedTestSocket.addSpeedTestListener(object : ISpeedTestListener {
                override fun onCompletion(report: SpeedTestReport) {
                    val result = report.transferRateBit.toDouble()/1000000
                    Log.d("complete => ", result.toString())
                }

                override fun onProgress(percent: Float, report: SpeedTestReport?) {
                    Log.d("progress ${report?.speedTestMode?.name}=> ", percent.toString())
                }

                override fun onError(speedTestError: SpeedTestError, errorMessage: String) {
                    Log.d("error => ", speedTestError.name)
                }
            })

            speedTestSocket.startDownload("http://speedtest.tele2.net/10MB.zip")
        }

        downloadSpeed
    }

    suspend fun performUploadTest(): BigDecimal {
        return withContext(Dispatchers.IO) {
            val socket = SpeedTestSocket()

            val deferred = CompletableDeferred<BigDecimal>()

            socket.startUpload("http://speedtest.tele2.net/upload.php", 1000000)

            socket.addSpeedTestListener(object : ISpeedTestListener {
                override fun onCompletion(report: SpeedTestReport) {
                    Log.d("Complete => ", report.transferRateBit.divide(BigDecimal.valueOf(1000000)).toString())
                }

                override fun onError(speedTestError: SpeedTestError, errorMessage: String) {
                    Log.d("Error => ", errorMessage)
                }

                override fun onProgress(percent: Float, report: SpeedTestReport) {
                    Log.d("Progress ${report.speedTestMode.name} => ", percent.toString())
                }
            })

            deferred.await()
        }
    }

}